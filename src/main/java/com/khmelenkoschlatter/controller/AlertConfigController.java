package com.khmelenkoschlatter.controller;

import com.google.gson.Gson;
import com.khmelenkoschlatter.model.Account;
import com.khmelenkoschlatter.model.AlertConfig;
import com.khmelenkoschlatter.model.Sensor;
import com.khmelenkoschlatter.repository.AccountRepository;
import com.khmelenkoschlatter.repository.AlertConfigRepository;
import com.khmelenkoschlatter.repository.SensorRepository;
import com.khmelenkoschlatter.repository.WeatherDataRepository;
import com.khmelenkoschlatter.request.AlertConfigRequest;
import com.khmelenkoschlatter.request.UpdateAlertConfigRequest;
import com.khmelenkoschlatter.response.BaseResponse;
import com.khmelenkoschlatter.response.WeatherDataHistoryResponse;
import com.khmelenkoschlatter.response.model.AlertConfigResponse;
import com.khmelenkoschlatter.translator.ResponseTranslator;
import com.khmelenkoschlatter.utils.HttpHeaderUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Donga on 03.10.2015.
 */
@RestController
@EnableAutoConfiguration
public class AlertConfigController {

    private static final String GET_ALERT_CONFIG = "/alertconfig";
    private static final String UPDATE_ALERT_CONFIG = "/updatealertconfig";

    Gson gson = new Gson();

    @Autowired
    WeatherDataRepository weatherDataRepository;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    SensorRepository sensorRepository;

    @Autowired
    AlertConfigRepository alertConfigRepository;

    @RequestMapping(value = GET_ALERT_CONFIG, method = RequestMethod.POST)
    String getAlertConfig(@RequestBody String alertConfigRequestString, @RequestHeader HttpHeaders headers) {

        String email = HttpHeaderUtils.getEmailFromHeaders(headers);
        Account userAccount = accountRepository.findOneByEmail(email);

        if (userAccount == null) {
            return gson.toJson(new BaseResponse(BaseResponse.FAILURE_AUTHENTICATION));
        }

        //TODO: Check if that user is allowed to get the alertConfig

        AlertConfigRequest alertConfigRequest = gson.fromJson(alertConfigRequestString, AlertConfigRequest.class);

        if (alertConfigRequest.getDeviceId() == null || alertConfigRequest.getDeviceId().length() == 0) {
            return gson.toJson(new BaseResponse(BaseResponse.FAILURE_RESSOURCE_NOT_EXISTS));
        }

        Sensor sensor = sensorRepository.findOneByClientId(alertConfigRequest.getDeviceId());
        AlertConfig alertConfig = alertConfigRepository.findOneBySensor(sensor);
        return gson.toJson(ResponseTranslator.ALERT_CONFIG_TRANSLATOR.apply(alertConfig));
    }

    @RequestMapping(value = UPDATE_ALERT_CONFIG, method = RequestMethod.POST)
    String updateAlertConfig(@RequestBody String updateAlertConfigRequestString, @RequestHeader HttpHeaders headers) {

        String email = HttpHeaderUtils.getEmailFromHeaders(headers);
        Account userAccount = accountRepository.findOneByEmail(email);

        if (userAccount == null) {
            return gson.toJson(new BaseResponse(BaseResponse.FAILURE_AUTHENTICATION));
        }

        UpdateAlertConfigRequest updateAlertConfigRequest = gson.fromJson(updateAlertConfigRequestString, UpdateAlertConfigRequest.class);

        if (updateAlertConfigRequest.getClientId() == null || updateAlertConfigRequest.getClientId().length() == 0 || updateAlertConfigRequest.getAlertConfigResponse() == null) {
            return gson.toJson(new WeatherDataHistoryResponse(BaseResponse.FAILURE_INVALID_REQUEST));
        }

        //TODO: Check if that user is allowed to update that AlertConfig

        Sensor sensor = sensorRepository.findOneByClientId(updateAlertConfigRequest.getClientId());
        AlertConfig alertConfig = alertConfigRepository.findOneBySensor(sensor);

        AlertConfigResponse alertConfigResponse = updateAlertConfigRequest.getAlertConfigResponse();
        //Take over the id in order to update the row
        AlertConfig newConfig = new AlertConfig(sensor);
        newConfig.setId(alertConfig.getId());
        newConfig.setIsTemperatureAlertEnabled(alertConfigResponse.isTemperatureAlertEnabled());
        newConfig.setMinTemperature(alertConfigResponse.getMinTemperature());
        newConfig.setMaxTemperature(alertConfigResponse.getMaxTemperature());
        newConfig.setIsHumidityAlertEnabled(alertConfigResponse.isHumidityAlertEnabled());
        newConfig.setMinHumidity(alertConfigResponse.getMinHumidity());
        newConfig.setMaxHumidity(alertConfigResponse.getMaxHumidity());
        newConfig = alertConfigRepository.save(newConfig);

        return gson.toJson(ResponseTranslator.ALERT_CONFIG_TRANSLATOR.apply(alertConfig));
    }

}
