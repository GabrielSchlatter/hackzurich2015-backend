package com.khmelenkoschlatter.controller;

import com.google.gson.Gson;
import com.khmelenkoschlatter.GCMService;
import com.khmelenkoschlatter.model.Account;
import com.khmelenkoschlatter.model.AlertConfig;
import com.khmelenkoschlatter.model.Sensor;
import com.khmelenkoschlatter.model.WeatherData;
import com.khmelenkoschlatter.repository.AccountRepository;
import com.khmelenkoschlatter.repository.AlertConfigRepository;
import com.khmelenkoschlatter.repository.SensorRepository;
import com.khmelenkoschlatter.repository.WeatherDataRepository;
import com.khmelenkoschlatter.request.AddSensorRequest;
import com.khmelenkoschlatter.request.WeatherDataHistoryRequest;
import com.khmelenkoschlatter.request.WeatherDataUpdateRequest;
import com.khmelenkoschlatter.response.BaseResponse;
import com.khmelenkoschlatter.response.WeatherDataHistoryResponse;
import com.khmelenkoschlatter.translator.ResponseTranslator;
import com.khmelenkoschlatter.utils.HttpHeaderUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Donga on 03.10.2015.
 */

@RestController
@EnableAutoConfiguration
public class SensorController {

    private static final String UPDATE = "/update";
    private static final String HISTORY = "/history";
    private static final String SENSORS = "/sensors";
    private static final String ADD_SENSOR = "/addsensor";

    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    Gson gson = new Gson();

    @Autowired
    WeatherDataRepository weatherDataRepository;

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    SensorRepository sensorRepository;

    @Autowired
    AlertConfigRepository alertConfigRepository;

    @Autowired
    GCMService notificationService;

    @RequestMapping(value = UPDATE, method = RequestMethod.POST)
    String update(@RequestBody String weatherDataUpdateRequestString) {

        System.out.println(weatherDataUpdateRequestString);
        WeatherDataUpdateRequest weatherDataUpdateRequest = gson.fromJson(weatherDataUpdateRequestString, WeatherDataUpdateRequest.class);

        Sensor sensor = sensorRepository.findOneByClientId(weatherDataUpdateRequest.getClientId());
        if (sensor == null) {
            //Sensor is not registered yet, discard the data
            return gson.toJson(new BaseResponse(BaseResponse.FAILURE_RESSOURCE_NOT_EXISTS));
        }

        if (weatherDataUpdateRequest.getClientId() == null || weatherDataUpdateRequest.getClientId().length() == 0) {
            //Invalid request
            return gson.toJson(new BaseResponse(BaseResponse.FAILURE_INVALID_REQUEST));
        }

        AlertConfig sensorConfig = alertConfigRepository.findOneBySensor(sensor);
        if (sensorConfig != null) {
            if (sensorConfig.isTemperatureAlertEnabled()) {
                if (sensorConfig.getMaxTemperature() < Double.parseDouble(weatherDataUpdateRequest.getTemperature())) {
                    notificationService.send(sensor.getAccount().getGcmToken(), sensor.getClientId(), "Temperature too high");
                } else if (sensorConfig.getMinTemperature() > Double.parseDouble(weatherDataUpdateRequest.getTemperature())) {
                    notificationService.send(sensor.getAccount().getGcmToken(), sensor.getClientId(), "Temperature too low");
                }
            }
            if (sensorConfig.isHumidityAlertEnabled()) {
                if (sensorConfig.getMaxHumidity() < Double.parseDouble(weatherDataUpdateRequest.getHumidity())) {
                    notificationService.send(sensor.getAccount().getGcmToken(), sensor.getClientId(), "Humidity too high");
                } else if (sensorConfig.getMaxHumidity() > Double.parseDouble(weatherDataUpdateRequest.getHumidity())) {
                    notificationService.send(sensor.getAccount().getGcmToken(), sensor.getClientId(), "Humidity too low");
                }
            }
        }

        Calendar cal = Calendar.getInstance();
        WeatherData newData = new WeatherData(sensor, dateFormat.format(cal.getTime()), weatherDataUpdateRequest.getClientId(), weatherDataUpdateRequest.getTemperature(), weatherDataUpdateRequest.getHumidity());
        weatherDataRepository.save(newData);

        return gson.toJson(new BaseResponse(BaseResponse.SUCCESS));
    }

    @RequestMapping(value = HISTORY, method = RequestMethod.POST)
    String history(@RequestBody String weatherDataHistoryRequestString, @RequestHeader HttpHeaders headers) {

        String email = HttpHeaderUtils.getEmailFromHeaders(headers);
        Account userAccount = accountRepository.findOneByEmail(email);

        //TODO: Check if user is allowed to fetch target sensors history

        System.out.println(weatherDataHistoryRequestString);
        WeatherDataHistoryRequest weatherDataHistoryRequest = gson.fromJson(weatherDataHistoryRequestString, WeatherDataHistoryRequest.class);

        if (weatherDataHistoryRequest.getSensorId() == null || weatherDataHistoryRequest.getSensorId().length() == 0) {
            //Invalid request
            return gson.toJson(new WeatherDataHistoryResponse(BaseResponse.FAILURE_INVALID_REQUEST));
        }

        List<WeatherData> weatherData = (List<WeatherData>) weatherDataRepository.findByClientId(weatherDataHistoryRequest.getSensorId());
        return gson.toJson(weatherData.stream().map(ResponseTranslator.WEATHER_DATA_TRANSLATOR).collect(Collectors.toList()));
    }

    @RequestMapping(value = SENSORS, method = RequestMethod.GET)
    String sensors(@RequestHeader HttpHeaders headers) {

        String email = HttpHeaderUtils.getEmailFromHeaders(headers);
        Account userAccount = accountRepository.findOneByEmail(email);

        if (userAccount == null) {
            return gson.toJson(new BaseResponse(BaseResponse.FAILURE_AUTHENTICATION));
        }

        List<Sensor> sensors = sensorRepository.findByAccount(userAccount);
        return gson.toJson(sensors.stream().map(ResponseTranslator.SENSOR_TRANSLATOR).collect(Collectors.toList()));
    }

    @RequestMapping(value = ADD_SENSOR, method = RequestMethod.POST)
    String addSensor(@RequestBody String AddSensorRequestString, @RequestHeader HttpHeaders headers) {

        String email = HttpHeaderUtils.getEmailFromHeaders(headers);
        Account userAccount = accountRepository.findOneByEmail(email);

        if (userAccount == null) {
            return gson.toJson(new BaseResponse(BaseResponse.FAILURE_AUTHENTICATION));
        }

        AddSensorRequest addSensorRequest = gson.fromJson(AddSensorRequestString, AddSensorRequest.class);
        Sensor newSensor = new Sensor(userAccount, addSensorRequest.getClientId());
        sensorRepository.save(newSensor);

        AlertConfig sensorConfig = new AlertConfig(newSensor);
        alertConfigRepository.save(sensorConfig);

        return gson.toJson(sensorRepository.findByAccount(userAccount).stream().map(ResponseTranslator.SENSOR_TRANSLATOR).collect(Collectors.toList()));
    }
}
