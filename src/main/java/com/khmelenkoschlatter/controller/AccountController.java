package com.khmelenkoschlatter.controller;

import com.google.gson.Gson;
import com.khmelenkoschlatter.model.Account;
import com.khmelenkoschlatter.request.LoginRequest;
import com.khmelenkoschlatter.response.LoginResponse;
import com.khmelenkoschlatter.response.model.AccountResponse;
import com.khmelenkoschlatter.response.BaseResponse;
import com.khmelenkoschlatter.response.RegistrationResponse;
import com.khmelenkoschlatter.translator.ResponseTranslator;
import com.khmelenkoschlatter.utils.HttpHeaderUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.*;
import org.springframework.http.HttpHeaders;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import com.khmelenkoschlatter.repository.AccountRepository;
import com.khmelenkoschlatter.request.RegistrationRequest;

@RestController
@EnableAutoConfiguration
public class AccountController {

    public static final String REGISTER = "/register";
    public static final String LOGIN = "/login";
    private static final String GCM_TOKEN = "/gcmtoken";

    private Gson gson = new Gson();

    @Autowired
    AccountRepository accountRepository;

    @RequestMapping(value = REGISTER, method = RequestMethod.POST)
    String register(@RequestBody String registrationRequestString) {

        RegistrationRequest registrationRequest = gson.fromJson(registrationRequestString, RegistrationRequest.class);

        if (registrationRequest.getEmail() == null || registrationRequest.getEmail().length() == 0 || registrationRequest.getPassword() == null || registrationRequest.getPassword().length() == 0) {
            //Invalid request
            RegistrationResponse response = new RegistrationResponse(BaseResponse.FAILURE_INVALID_REQUEST);
            return gson.toJson(response);
        }

        Account existingAccount = accountRepository.findOneByEmail(registrationRequest.getEmail());
        if (existingAccount != null) {
            //Account already exists
            RegistrationResponse response = new RegistrationResponse(BaseResponse.FAILURE_RESSOURCE_EXISTS);
            return gson.toJson(response);
        }

        String passwordHash = new BCryptPasswordEncoder().encode(registrationRequest.getPassword());
        Account newAccount = new Account(registrationRequest.getEmail(), passwordHash);
        accountRepository.save(newAccount);

        AccountResponse accountResponse = new AccountResponse(registrationRequest.getEmail(), registrationRequest.getPassword());
        RegistrationResponse registrationResponse = new RegistrationResponse(accountResponse);
        return gson.toJson(registrationResponse);
    }

    @RequestMapping(value = LOGIN, method = RequestMethod.POST)
    public String login(@RequestBody String loginRequestString) {

        LoginRequest loginRequest = gson.fromJson(loginRequestString, LoginRequest.class);
        Account existingAccount = accountRepository.findOneByEmail(loginRequest.getEmail());

        if (existingAccount == null) {
            return gson.toJson(new LoginResponse(LoginResponse.FAILURE_RESSOURCE_NOT_EXISTS));
        } else {
            if (new BCryptPasswordEncoder().matches(loginRequest.getPassword(), existingAccount.getPasswordHash())) {
                return gson.toJson(new LoginResponse(ResponseTranslator.ACCOUNT_TRANSLATOR.apply(new Account(loginRequest.getEmail(), loginRequest.getPassword()))));
            } else {
                //Wrong password
                return gson.toJson(new LoginResponse(LoginResponse.FAILURE_AUTHENTICATION));
            }
        }
    }

    @RequestMapping(value = GCM_TOKEN, method = RequestMethod.POST)
    public String registerGcmToken(@RequestBody String token, @RequestHeader HttpHeaders headers) {

        System.out.println("Received Token: " + token);

        String email = HttpHeaderUtils.getEmailFromHeaders(headers);
        Account userAccount = accountRepository.findOneByEmail(email);

        if (userAccount == null) {
            return "FAILURE";
        }

        userAccount.setGcmToken(token.replace("\"",""));
        accountRepository.save(userAccount);
        return "SUCCESS";
    }
}