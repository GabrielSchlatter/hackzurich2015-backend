package com.khmelenkoschlatter.response.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Donga on 03.10.2015.
 */
public class WeatherDataResponse {

    @SerializedName("clientId")
    private String clientId;

    @SerializedName("timestamp")
    private String timestamp;

    @SerializedName("temperature")
    private String temperature;

    @SerializedName("humidity")
    private String humidity;

    public WeatherDataResponse() {

    }

    public WeatherDataResponse(String clientId, String timestamp, String temperature, String humidity) {
        this.clientId = clientId;
        this.timestamp = timestamp;
        this.temperature = temperature;
        this.humidity = humidity;
    }

    public String getClientId() {
        return clientId;
    }

    public String getTemperature() {
        return temperature;
    }

    public String getHumidity() {
        return humidity;
    }

    public String getTimestamp() {
        return timestamp;
    }
}
