package com.khmelenkoschlatter.response;

import com.khmelenkoschlatter.response.model.WeatherDataResponse;

import java.util.List;

/**
 * Created by Donga on 03.10.2015.
 */
public class WeatherDataHistoryResponse extends BaseResponse {

    private List<WeatherDataResponse> history;

    public WeatherDataHistoryResponse(int result) {
        super(result);
    }

    public WeatherDataHistoryResponse(List<WeatherDataResponse> history) {
        super(SUCCESS);
        this.history = history;
    }

    public List<WeatherDataResponse> getHistory() {
        return history;
    }
}
