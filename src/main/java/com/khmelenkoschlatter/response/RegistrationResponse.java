package com.khmelenkoschlatter.response;

import com.google.gson.annotations.SerializedName;
import com.khmelenkoschlatter.response.model.AccountResponse;

/**
 * Created by Donga on 03.10.2015.
 */
public class RegistrationResponse extends BaseResponse {

    @SerializedName("accountResponse")
    private AccountResponse accountResponse;

    public RegistrationResponse(int result) {
        super(result);
    }

    public RegistrationResponse(AccountResponse accountResponse) {
        super(SUCCESS);
        this.accountResponse = accountResponse;
    }

    public AccountResponse getAccountResponse() {
        return accountResponse;
    }
}
