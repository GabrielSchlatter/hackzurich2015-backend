package com.khmelenkoschlatter.utils;

import org.springframework.http.HttpHeaders;

import java.nio.charset.Charset;
import java.util.Base64;

public final class HttpHeaderUtils {

    public static String getEmailFromHeaders(HttpHeaders headers) {
        String authorization = headers.getFirst("Authorization");
        if (authorization != null && authorization.startsWith("Basic")) {
            // Authorization: Basic base64credentials
            String base64Credentials = authorization.substring("Basic".length()).trim();
            String credentials = new String(Base64.getDecoder().decode(base64Credentials),
                    Charset.forName("UTF-8"));
            // credentials = username:password
            final String[] values = credentials.split(":", 2);
            if (values.length == 2) {
                return values[0];
            }
        }
        return null;
    }
}
