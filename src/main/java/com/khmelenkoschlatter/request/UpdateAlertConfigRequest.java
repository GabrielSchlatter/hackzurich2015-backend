package com.khmelenkoschlatter.request;

import com.google.gson.annotations.SerializedName;
import com.khmelenkoschlatter.response.model.AlertConfigResponse;

/**
 * Created by Donga on 03.10.2015.
 */
public class UpdateAlertConfigRequest {

    @SerializedName("clientId")
    private String clientId;

    @SerializedName("alertConfigResponse")
    private AlertConfigResponse alertConfigResponse;

    public UpdateAlertConfigRequest() {

    }

    public UpdateAlertConfigRequest(String clientId, AlertConfigResponse alertConfigResponse) {
        this.clientId = clientId;
        this.alertConfigResponse = alertConfigResponse;
    }

    public String getClientId() {
        return clientId;
    }

    public AlertConfigResponse getAlertConfigResponse() {
        return alertConfigResponse;
    }
}
