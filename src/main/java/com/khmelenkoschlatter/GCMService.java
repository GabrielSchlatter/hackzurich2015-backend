package com.khmelenkoschlatter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.*;

@Service
public class GCMService {

    private String gcmUri = "https://gcm-http.googleapis.com/gcm/send";
    private String gcmUri1 = "https://android.googleapis.com/gcm/send\"";
    private String gcmApiKey = "AIzaSyBIZKCY3nhdjLB-rg5nsLNh4uyX5L7gKYs";

    public void send(String regId, String sensorId, String message) {
        Content content = new Content();
        //POJO class as above for standard message format
        content.addRegId(regId);
        System.out.println("Using token for sending: " + regId);
        content.createData("Title", "Notification Message");

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        headers.set("Authorization", "key=" + gcmApiKey);

        System.out.println(new Gson().toJson(content));
        HttpEntity request = new HttpEntity(content, headers);

        System.out.println(request.getHeaders().toString());
        System.out.println(request.getBody().toString());

        ResponseEntity<String> result = restTemplate.exchange(gcmUri1, HttpMethod.POST, request, String.class);
        System.out.println(result);
    }

    public class Content implements Serializable {

        public String to;
        public Map<String, String> data;

        public void addRegId(String regId) {
            to = regId;
        }

        public void createData(String title, String message) {
            if (data == null)
                data = new HashMap<String, String>();

            data.put("title", title);
            data.put("message", message);
        }

    }
}