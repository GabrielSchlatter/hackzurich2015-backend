package com.khmelenkoschlatter.repository;

import com.khmelenkoschlatter.model.Account;
import com.khmelenkoschlatter.model.Sensor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Donga on 03.10.2015.
 */
public interface SensorRepository extends CrudRepository<Sensor, Long> {


    Sensor findOneByClientId(String clientId);

    List<Sensor> findByAccount(Account userAccount);
}
