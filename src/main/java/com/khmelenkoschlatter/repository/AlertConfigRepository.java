package com.khmelenkoschlatter.repository;

import com.khmelenkoschlatter.model.AlertConfig;
import com.khmelenkoschlatter.model.Sensor;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Donga on 03.10.2015.
 */
public interface AlertConfigRepository extends CrudRepository<AlertConfig, Long> {

    AlertConfig findOneBySensor(Sensor sensor);
}
