package com.khmelenkoschlatter.repository;

import com.khmelenkoschlatter.model.Account;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Donga on 03.10.2015.
 */
public interface AccountRepository extends CrudRepository<Account, Long> {

    Account findOneByEmail(String username);
}