package com.khmelenkoschlatter.repository;

import com.khmelenkoschlatter.model.WeatherData;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by Donga on 03.10.2015.
 */
public interface WeatherDataRepository extends CrudRepository<WeatherData, Long> {

    List<WeatherData> findByClientId(String clientId);
}
