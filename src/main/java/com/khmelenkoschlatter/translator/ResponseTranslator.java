package com.khmelenkoschlatter.translator;

import com.khmelenkoschlatter.model.Account;
import com.khmelenkoschlatter.model.AlertConfig;
import com.khmelenkoschlatter.model.Sensor;
import com.khmelenkoschlatter.model.WeatherData;
import com.khmelenkoschlatter.response.model.AccountResponse;
import com.khmelenkoschlatter.response.model.AlertConfigResponse;
import com.khmelenkoschlatter.response.model.SensorResponse;
import com.khmelenkoschlatter.response.model.WeatherDataResponse;

import java.util.function.Function;

/**
 * Created by Donga on 03.10.2015.
 */
public interface ResponseTranslator {

    public static Function<Account, AccountResponse> ACCOUNT_TRANSLATOR = (account) -> {
        return new AccountResponse(account.getEmail(), account.getPasswordHash());
    };

    public static Function<WeatherData, WeatherDataResponse> WEATHER_DATA_TRANSLATOR = (data) -> {
        return new WeatherDataResponse(data.getClientId(), data.getTimestamp(), data.getTemperature(), data.getHumidity());
    };

    public static Function<Sensor, SensorResponse> SENSOR_TRANSLATOR = (sensor) -> {
        return new SensorResponse(sensor.getClientId());
    };

    public static Function<AlertConfig, AlertConfigResponse> ALERT_CONFIG_TRANSLATOR = (config) -> {
        AlertConfigResponse response = new AlertConfigResponse();
        response.setIsTemperatureAlertEnabled(config.isTemperatureAlertEnabled());
        response.setMinTemperature(config.getMinTemperature());
        response.setMaxTemperature(config.getMaxTemperature());
        response.setIsHumidityAlertEnabled(config.isHumidityAlertEnabled());
        response.setMinHumidity(config.getMinHumidity());
        response.setMaxHumidity(config.getMaxHumidity());
        return response;
    };
}
