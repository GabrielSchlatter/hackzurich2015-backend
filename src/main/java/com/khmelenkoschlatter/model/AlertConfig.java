package com.khmelenkoschlatter.model;

import javax.persistence.*;

/**
 * Created by Donga on 03.10.2015.
 */
@Entity
public class AlertConfig {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToOne
    @JoinColumn(name = "sensorId")
    private Sensor sensor;

    private boolean isTemperatureAlertEnabled;

    private double minTemperature;

    private double maxTemperature;

    private boolean isHumidityAlertEnabled;

    private double minHumidity;

    private double maxHumidity;

    public AlertConfig() {

    }

    public AlertConfig(Sensor sensor) {
        this.sensor = sensor;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isTemperatureAlertEnabled() {
        return isTemperatureAlertEnabled;
    }

    public void setIsTemperatureAlertEnabled(boolean isTemperatureAlertEnabled) {
        this.isTemperatureAlertEnabled = isTemperatureAlertEnabled;
    }

    public double getMinTemperature() {
        return minTemperature;
    }

    public void setMinTemperature(double minTemperature) {
        this.minTemperature = minTemperature;
    }

    public double getMaxTemperature() {
        return maxTemperature;
    }

    public void setMaxTemperature(double maxTemperature) {
        this.maxTemperature = maxTemperature;
    }

    public boolean isHumidityAlertEnabled() {
        return isHumidityAlertEnabled;
    }

    public void setIsHumidityAlertEnabled(boolean isHumidityAlertEnabled) {
        this.isHumidityAlertEnabled = isHumidityAlertEnabled;
    }

    public double getMinHumidity() {
        return minHumidity;
    }

    public void setMinHumidity(double minHumidity) {
        this.minHumidity = minHumidity;
    }

    public double getMaxHumidity() {
        return maxHumidity;
    }

    public void setMaxHumidity(double maxHumidity) {
        this.maxHumidity = maxHumidity;
    }
}
