package com.khmelenkoschlatter.model;

import javax.persistence.*;

/**
 * Created by Donga on 03.10.2015.
 */
@Entity
public class WeatherData {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne
    @JoinColumn(name = "sensorId")
    private Sensor sensor;

    private String timestamp;

    private String clientId;

    private String temperature;

    private String humidity;

    public WeatherData() {

    }

    public WeatherData(Sensor sensor, String timestamp, String clientId, String temperature, String humidity) {
        this.sensor = sensor;
        this.timestamp = timestamp;
        this.clientId = clientId;
        this.temperature = temperature;
        this.humidity = humidity;
    }

    public String getClientId() {
        return clientId;
    }

    public String getTemperature() {
        return temperature;
    }

    public String getHumidity() {
        return humidity;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
