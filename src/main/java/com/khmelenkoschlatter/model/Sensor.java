package com.khmelenkoschlatter.model;

import javax.persistence.*;

/**
 * Created by Donga on 03.10.2015.
 */
@Entity
public class Sensor {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne
    @JoinColumn(name = "accountId")
    private Account account;

    private String clientId;

    public Sensor() {

    }

    public Sensor(Account account, String deviceId) {
        this.account = account;
        this.clientId = deviceId;
    }

    public String getClientId() {
        return clientId;
    }

    public Account getAccount() {
        return account;
    }
}
